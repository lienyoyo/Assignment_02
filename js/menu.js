var menuState = {
    preload : function(){


        
    },

    create : function(){
        
        this.menu_bg = game.add.sprite(game.width/2, game.height/2, 'menu_bg');
        this.menu_bg.anchor.setTo(0.5, 0.5);


        this.title = game.add.text(game.width/2, 150, 'Raiden', {font: '50px Arial', fill: '#ffffff'});
        this.title.anchor.setTo(0.5, 0.5);


        var btn_play_pos = {x: game.width/4, y: game.height-100};
        var btn_play = game.add.button(btn_play_pos.x, btn_play_pos.y, 'buttons', this.click_btn_play, this, 2, 0, 3);
        btn_play.anchor.setTo(0.5, 0.5);
        var btn_play_text = game.add.text(btn_play_pos.x, btn_play_pos.y, 'Play', {font: '30px Arial', fill: '#ffffff'});
        btn_play_text.anchor.setTo(0.5, 0.5);


        var btn_option_pos = {x: game.width/4*3, y: game.height-100};
        var btn_option = game.add.button(btn_option_pos.x, btn_option_pos.y, 'buttons', this.click_btn_option, this, 2, 0, 3);
        btn_option.anchor.setTo(0.5, 0.5);
        var btn_option_text = game.add.text(btn_option_pos.x, btn_option_pos.y, 'Option', {font: '30px Arial', fill: '#ffffff'});
        btn_option_text.anchor.setTo(0.5, 0.5);


        var btn_score_pos = {x: game.width/2, y: game.height-50};
        var btn_score = game.add.button(btn_score_pos.x, btn_score_pos.y, 'buttons', this.click_btn_score, this, 2, 0, 3);
        btn_score.anchor.setTo(0.5, 0.5);
        var btn_score_text = game.add.text(btn_score_pos.x, btn_score_pos.y, 'Leader Board', {font: '30px Arial', fill: '#ffffff'});
        btn_score_text.anchor.setTo(0.5, 0.5);


        
        
    },

    update : function(){

        if(!game.global.menubgm.isPlaying){
            game.global.menubgm.play();
        }

    },




    click_btn_play : function(){


        game.state.start('menu_play');
    },

    click_btn_option : function(){


        game.state.start('option');
    },

    click_btn_score : function(){


        game.state.start('score');
    },
}