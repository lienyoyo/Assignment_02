var gameoverState = {
    preload : function(){


        
    },

    create : function(){
        // var test_text = game.add.text(game.width/2, 150, 'gameover', {font: '30px Arial', fill: '#ffffff'});
        // test_text.anchor.setTo(0.5, 0.5);



        this.gameover_bg = game.add.sprite(game.width/2, game.height/2, 'gameover_bg');
        this.gameover_bg.anchor.setTo(0.5, 0.5);
        // this.gameover_bg.scale.setTo(2.2, 2.2);


        this.gameover_img = game.add.sprite(game.width/2, 0, 'gameover_img');
        this.gameover_img.anchor.setTo(0.5, 0);
        this.gameover_img.scale.setTo(2.2, 2.2);
        this.gameover_img.alpha = 0;
        this.gameover_img.animations.add('ani', Phaser.ArrayUtils.numberArray(0,48), 16, false);
        

        this.text_youdied = game.add.text(game.width/2, 265, 'You Died', {font: '40px Arial', fill: '#ffffff'});
        this.text_youdied.anchor.setTo(0.5, 0.5);

        this.text_gameover = game.add.text(game.width/2, game.height/2 + 50, 'Game Over', {font: '50px Arial', fill: '#ffffff'});
        this.text_gameover.anchor.setTo(0.5, 0.5);

        this.text_score = game.add.text(game.width/2, game.height/2 + 120, 'Score: ' + game.global.score, {font: '30px Arial', fill: '#ffffff'});
        this.text_score.anchor.setTo(0.5, 0.5);





        this.btn_play_pos = {x: game.width/4, y: game.height-70};
        this.btn_play = game.add.button(this.btn_play_pos.x, this.btn_play_pos.y, 'buttons', this.click_btn_play, this, 2, 0, 3);
        this.btn_play.anchor.setTo(0.5, 0.5);

        this.btn_play_text = game.add.text(this.btn_play_pos.x, this.btn_play_pos.y, 'Play Again', {font: '30px Arial', fill: '#ffffff'});
        this.btn_play_text.anchor.setTo(0.5, 0.5);


        this.btn_menu_pos = {x: game.width/4*3, y: game.height-70};
        this.btn_menu = game.add.button(this.btn_menu_pos.x, this.btn_menu_pos.y, 'buttons', this.click_btn_menu, this, 2, 0, 3);
        this.btn_menu.anchor.setTo(0.5, 0.5);
        this.btn_menu_text = game.add.text(this.btn_menu_pos.x, this.btn_menu_pos.y, 'Menu', {font: '30px Arial', fill: '#ffffff'});
        this.btn_menu_text.anchor.setTo(0.5, 0.5);




        this.fade_in = game.add.tween(this.gameover_img);
        this.fade_in.to( { alpha: 1 }, 1000);
        this.fade_out = game.add.tween(this.gameover_img);
        this.fade_out.to( { alpha: 0 }, 1000);

        this.text_youdied_fade_in = game.add.tween(this.text_youdied);
        this.text_youdied_fade_in.to( { alpha: 1 }, 1000);
        this.text_youdied_fade_out = game.add.tween(this.text_youdied);
        this.text_youdied_fade_out.to( { alpha: 0 }, 1000);

    
        this.btn_send_pos = {x: game.width/2, y: game.height-150};
        this.btn_send = game.add.button(this.btn_send_pos.x, this.btn_send_pos.y, 'buttons', this.click_btn_send, this, 2, 0, 3);
        this.btn_send.anchor.setTo(0.5, 0.5);
        this.btn_send_text = game.add.text(this.btn_send_pos.x, this.btn_send_pos.y, 'Send score', {font: '30px Arial', fill: '#ffffff'});
        this.btn_send_text.anchor.setTo(0.5, 0.5);
        

        this.text_Sending = game.add.text(game.width/2, game.height - 110, 'Sending', {font: '30px Arial', fill: '#ffffff'});
        this.text_Sending.anchor.setTo(0.5, 0.5);
        this.text_Sending.exists = false;




        this.fadein();
    },

    update : function(){
        if(!game.global.menubgm.isPlaying){
            game.global.menubgm.play();
        }
        if(game.global.gamebgm.isPlaying){
            game.global.gamebgm.stop();
        }
        if(game.global.bossbgm.isPlaying){
            game.global.bossbgm.stop();
        }
    },


    click_btn_send: function(){
        var input = document.getElementById('input');
        var name = input.value;
        var show = this.text_Sending;
        show.exists = true;


        var postsRef = firebase.database().ref('leaderboard');
        postsRef.push().set({
            name: name,
            score: game.global.score,
        }).then(function(){
            show.text = 'Sending complete';
        }).catch(function(e){
            console.log(e);
        });

    },

    fadeout : function(){
        this.fade_out.start();
        this.text_youdied_fade_out.start();

        game.time.events.add(2500, this.fadein, this);
    },

    fadein : function(){
        this.fade_in.start();
        this.text_youdied_fade_in.start();
        this.gameover_img.play('ani');

        game.time.events.add(2300, this.fadeout, this);
    },


    click_btn_play : function(){

        game.state.start(game.global.prev_play_state);
    },

    click_btn_menu : function(){

        game.state.start('menu');
    },

}