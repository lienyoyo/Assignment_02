var game_2p_HState = {
    preload : function(){},

    create : function(){
        // var test_text = game.add.text(game.width/2, 150, 'game 1p', {font: '30px Arial', fill: '#ffffff'});
        // test_text.anchor.setTo(0.5, 0.5);

        this.totalscore = 0;
        this.item_ring = false;
        this.item_ging = false;
        this.item_bing = false;
        // config
        /***********************************************************************/
        this.player_hp = 100;
        this.player_Invincible_delay = 1000;


        this.item_score = 100;
        this.item_score_acc = 0;


        this.power_number = 3;
        this.powering = false;


        this.bullet_auto_firingdelay = 1500;


        this.enemies_1_maxnumber = 30;
        this.enemies_1_MAXSPACING = 200;
        this.enemies_1_MINSPACING = 100;
        this.enemies_1_speed = 100;
        // this.enemies_1_turnspeed = 20;
        this.enemies_1_hp = 1;
        this.enemies_1_firingdelay = 1000;
        this.enemies_1_damage = 1;
        this.enemies_1_bulletSpeed = 300;
        this.enemies_1_score = 1;


        this.enemies_2_maxnumber = 10;
        this.enemies_2_MAXSPACING = 3500;
        this.enemies_2_MINSPACING = 3000;
        this.enemies_2_speed = 40;
        this.enemies_2_turnspeed = 20;
        this.enemies_2_hp = 30;
        this.enemies_2_firingdelay = 1500;
        this.enemies_2_damage = 5;
        this.enemies_2_bulletSpeed = 200;
        this.enemies_2_score = 10;


        this.totalscore_launchboss = 1000;
        // this.boss_maxnumber = 5;
        // this.boss_MAXSPACING = 4000;
        // this.boss_MINSPACING = 3500;
        // this.boss_speed = 40;
        // this.boss_turnspeed = 20;
        this.boss_hp_1 = 200;
        this.boss_hp_2 = 500;
        this.boss_hp_3 = 1000;
        this.boss_score_1 = 200;
        this.boss_score_2 = 200;
        this.boss_score_3 = 500;


        this.boss_damage_A = 15;
        this.boss_bulletSpeed_A = 150;
        this.boss_firingdelay_A = 2700;
        // this.boss_turnangle_A = 10;
        // this.boss_turnangleNOW_A = 0;
        
        
        this.boss_damage_B = 1;
        this.boss_bulletSpeed_B = 200;
        this.boss_firingdelay_B = 700;
        this.boss_turnangle_B = 10;
        this.boss_turnangleNOW_B = 0;


        this.boss_damage_C = 1;
        this.boss_bulletSpeed_C = 100;
        this.boss_firingdelay_C = 350;
        this.boss_turnangle_C = 10;
        this.boss_turnangleNOW_C = 0;
        this.boss_typedelay_C = 10000;
        this.boss_typekeep_C = 6000;






        this.shield_num = 100;
        this.explosion_num = 30;

        // bg
        /***********************************************************************/
        this.bg1 = game.add.tileSprite(game.width/2, 0, 600, 620, 'game_bg1');
        this.bg1.anchor.setTo(0.5, 0);
        this.bg2 = game.add.tileSprite(0, 0, 614, 1249, 'game_bg2');
        this.bg3 = game.add.tileSprite(0, 0, 625, 1278, 'game_bg3');



        // player
        /***********************************************************************/
        this.player = game.add.sprite(game.width/2-25, game.height-50, 'player');
        this.player.frame = 5;
        this.player.scale.setTo(0.5, 0.5);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.hp = this.player_hp;
        this.player.lastgethit = game.time.now;

        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        this.player2 = game.add.sprite(game.width/2+25, game.height-50, 'player');
        this.player2.frame = 5;
        this.player2.scale.setTo(0.5, 0.5);
        this.player2.anchor.setTo(0.5, 0.5);
        this.player2.hp = this.player_hp;
        this.player2.lastgethit = game.time.now;
        this.player2.tint = 0xff00ff;

        game.physics.arcade.enable(this.player2);
        this.player2.body.collideWorldBounds = true;



        this.helper = game.add.sprite(game.width/2, game.height/2, 'player');
        this.helper.frame = 5;
        this.helper.scale.setTo(0.7, 0.7);
        this.helper.anchor.setTo(0.5, 0.5);
        // this.helper.hp = this.player_hp;
        // this.helper.lastgethit = game.time.now;
        this.helper.exists = false;

        // game.physics.arcade.enable(this.player);
        // this.player.body.collideWorldBounds = true;

        this.helper2 = game.add.sprite(game.width/2, game.height/2, 'player');
        this.helper2.frame = 5;
        this.helper2.scale.setTo(0.7, 0.7);
        this.helper2.anchor.setTo(0.5, 0.5);
        // this.helper.hp = this.player_hp;
        // this.helper.lastgethit = game.time.now;
        this.helper2.exists = false;


        
        // boss
        /***********************************************************************/
        this.boss = game.add.sprite(game.width/2, -150, 'boss1');
        this.boss.frame = 0;
        this.boss.scale.setTo(0.75, 0.75);
        this.boss.anchor.setTo(0.5, 0.5);
        this.boss.angle = 180;
        this.boss.hp = this.boss_hp_1;

        
        // this.boss.firerate = this.enemies_1_firerate;
        this.boss.score = this.boss_score_1;
        // this.boss.lastgethit = game.time.now;

        game.physics.arcade.enable(this.boss);
        // this.boss.body.collideWorldBounds = true;

        this.boss.body.maxVelocity.x = 100;


        this.boss.particle = game.add.emitter(0, 0, 80);
        this.boss.particle.makeParticles('part_red');
        this.boss.particle.setYSpeed(-150, 150);
        this.boss.particle.setXSpeed(-150, 150);
        this.boss.particle.setScale(2, 0, 2, 0, 600);
        this.boss.particle.gravity = 400;

        // this.boss.last_hitbylight = game.time.now;
        // this.boss.type = 3;

        // this.boss.alive = false;
        this.boss.exists = false;

        this.boss.state = 1;
        this.boss.next_state = 2;
        this.boss.invincible = true;

        this.IslaunchBoss = false;


        // enemies
        /***********************************************************************/
        this.enemies_2 = game.add.group();
        this.enemies_2.enableBody = true;
        this.enemies_2.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemies_2.createMultiple(this.enemies_2_maxnumber, 'enemy_2');
        this.enemies_2.setAll('anchor.x', 0.5);
        this.enemies_2.setAll('anchor.y', 0.5);
        // this.enemies_2.setAll('scale.x', 1.5);
        // this.enemies_2.setAll('scale.y', 1.5);
        this.enemies_2.setAll('angle', 180);
        this.enemies_2.setAll('outOfBoundsKill', true);
        this.enemies_2.setAll('checkWorldBounds', true);
        // this.enemies_2.setAll('last_hitbylight', game.time.now);
        // this.enemies_2.setAll('type', 2);


        this.enemies_2.forEach(function (enemy) {
            enemy.particle = game.add.emitter(0, 0, 80);
            enemy.particle.makeParticles('part_red');
            enemy.particle.setYSpeed(-150, 150);
            enemy.particle.setXSpeed(-150, 150);
            enemy.particle.setScale(2, 0, 2, 0, 600);
            enemy.particle.gravity = 400;
            // enemy.last_hitbylight = 0;

        }, this);
        
        
        
        this.enemies_1 = game.add.group();
        this.enemies_1.enableBody = true;
        this.enemies_1.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemies_1.createMultiple(this.enemies_1_maxnumber, 'enemy_1');
        this.enemies_1.setAll('anchor.x', 0.5);
        this.enemies_1.setAll('anchor.y', 0.5);
        // this.enemies_1.setAll('scale.x', 1.5);
        // this.enemies_1.setAll('scale.y', 1.5);
        // this.enemies_1.setAll('angle', 180);
        this.enemies_1.setAll('outOfBoundsKill', true);
        this.enemies_1.setAll('checkWorldBounds', true);
        // this.enemies_1.setAll('last_hitbylight', game.time.now);
        this.enemies_1.setAll('type', 1);
        

        this.enemies_1.forEach(function (enemy) {
            enemy.particle = game.add.emitter(0, 0, 80);
            enemy.particle.makeParticles('part_red');
            enemy.particle.setYSpeed(-150, 150);
            enemy.particle.setXSpeed(-150, 150);
            enemy.particle.setScale(2, 0, 2, 0, 600);
            enemy.particle.gravity = 400;
            // enemy.last_hitbylight = 0;
        }, this);

        
        this.launchEnemy(1);
        this.launchEnemy(2);



        
        // bullet
        /***********************************************************************/
        this.bullet_red = game.add.weapon(40, 'bullet_red');
        this.bullet_red.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red.bulletSpeed = 400;
        this.bullet_red.fireRate = 40;
        this.bullet_red.bullets.setAll('damage', 1);
        this.bullet_red.bullets.setAll('alpha', 0.9);
        this.bullet_red.trackSprite(this.player, 0, -20);
    

        this.bullet_red_l = game.add.weapon(40, 'bullet_red');
        this.bullet_red_l.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_l.bulletSpeed = 400;
        this.bullet_red_l.fireRate = 40;
        this.bullet_red_l.fireAngle  = -135;
        this.bullet_red_l.bullets.setAll('damage', 1);
        this.bullet_red_l.bullets.setAll('alpha', 0.9);
        this.bullet_red_l.trackSprite(this.player, 0, -20);

        this.bullet_red_r = game.add.weapon(40, 'bullet_red');
        this.bullet_red_r.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_r.bulletSpeed = 400;
        this.bullet_red_r.fireRate = 40;
        this.bullet_red_r.fireAngle  = -45;
        this.bullet_red_r.bullets.setAll('damage', 1);
        this.bullet_red_r.bullets.setAll('alpha', 0.9);
        this.bullet_red_r.trackSprite(this.player, 0, -20);


        this.bullet_red_help = game.add.weapon(40, 'bullet_red');
        this.bullet_red_help.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_help.bulletSpeed = 400;
        this.bullet_red_help.fireRate = 40;
        this.bullet_red_help.bullets.setAll('damage', 1);
        this.bullet_red_help.bullets.setAll('alpha', 0.9);
        this.bullet_red_help.trackSprite(this.helper, 0, -20);


        this.bullet_red2 = game.add.weapon(40, 'bullet_red');
        this.bullet_red2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red2.bulletSpeed = 400;
        this.bullet_red2.fireRate = 40;
        this.bullet_red2.bullets.setAll('damage', 1);
        this.bullet_red2.bullets.setAll('alpha', 0.9);
        this.bullet_red2.trackSprite(this.player2, 0, -20);
    

        this.bullet_red_l2 = game.add.weapon(40, 'bullet_red');
        this.bullet_red_l2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_l2.bulletSpeed = 400;
        this.bullet_red_l2.fireRate = 40;
        this.bullet_red_l2.fireAngle  = -135;
        this.bullet_red_l2.bullets.setAll('damage', 1);
        this.bullet_red_l2.bullets.setAll('alpha', 0.9);
        this.bullet_red_l2.trackSprite(this.player2, 0, -20);

        this.bullet_red_r2 = game.add.weapon(40, 'bullet_red');
        this.bullet_red_r2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_r2.bulletSpeed = 400;
        this.bullet_red_r2.fireRate = 40;
        this.bullet_red_r2.fireAngle  = -45;
        this.bullet_red_r2.bullets.setAll('damage', 1);
        this.bullet_red_r2.bullets.setAll('alpha', 0.9);
        this.bullet_red_r2.trackSprite(this.player2, 0, -20);


        this.bullet_red_help2 = game.add.weapon(40, 'bullet_red');
        this.bullet_red_help2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet_red_help2.bulletSpeed = 400;
        this.bullet_red_help2.fireRate = 40;
        this.bullet_red_help2.bullets.setAll('damage', 1);
        this.bullet_red_help2.bullets.setAll('alpha', 0.9);
        this.bullet_red_help2.trackSprite(this.helper2, 0, -20);




    

        this.bullet_auto = game.add.group();
        this.bullet_auto.enableBody = true;
        this.bullet_auto.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullet_auto.createMultiple(80, 'bullet_auto');
        this.bullet_auto.setAll('anchor.x', 0.5);
        this.bullet_auto.setAll('anchor.y', 0.5);
        this.bullet_auto.setAll('alpha', 0.9);
        this.bullet_auto.setAll('outOfBoundsKill', true);
        this.bullet_auto.setAll('checkWorldBounds', true);
        this.bullet_auto.lastShot = 0;


        this.bullet_enemy = game.add.group();
        this.bullet_enemy.enableBody = true;
        this.bullet_enemy.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullet_enemy.createMultiple(2000, 'bullet_enemy');
        this.bullet_enemy.setAll('anchor.x', 0.5);
        this.bullet_enemy.setAll('anchor.y', 0.5);
        this.bullet_enemy.setAll('alpha', 0.9);
        this.bullet_enemy.setAll('outOfBoundsKill', true);
        this.bullet_enemy.setAll('checkWorldBounds', true);



        
        // this.lightening = game.add.sprite(0, 0, 'lightening');
        // this.lightening.frame = 0;
        // // this.lightening.scale.setTo(1, 0.5);
        // // this.lightening.angle = -90;
        // this.lightening.anchor.setTo(0.5, 1);
        // this.lightening.damage = this.lightening_damage;

        // game.physics.arcade.enable(this.lightening);
        // this.lightening.animations.add('anim', [0, 1, 2, 3], 8, true);

        // this.lightening.exists = false;


        this.item_r = game.add.group();
        this.item_r.enableBody = true;
        this.item_r.physicsBodyType = Phaser.Physics.ARCADE;
        this.item_r.createMultiple(5, 'item_r');
        this.item_r.setAll('anchor.x', 0.5);
        this.item_r.setAll('anchor.y', 0.5);
        this.item_r.setAll('checkWorldBounds', true);
        this.item_r.setAll('outOfBoundsKill', true);
        this.item_r.forEach( function(item) {
            item.animations.add('ani', [0, 1, 2, 3, 4, 5], 8, true);
        });
        
        
        this.item_g = game.add.group();
        this.item_g.enableBody = true;
        this.item_g.physicsBodyType = Phaser.Physics.ARCADE;
        this.item_g.createMultiple(5, 'item_g');
        this.item_g.setAll('anchor.x', 0.5);
        this.item_g.setAll('anchor.y', 0.5);
        this.item_g.setAll('checkWorldBounds', true);
        this.item_g.setAll('outOfBoundsKill', true);
        this.item_g.forEach( function(item) {
            item.animations.add('ani', [0, 1, 2, 3, 4, 5], 8, true);
        });


        this.item_b = game.add.group();
        this.item_b.enableBody = true;
        this.item_b.physicsBodyType = Phaser.Physics.ARCADE;
        this.item_b.createMultiple(5, 'item_b');
        this.item_b.setAll('anchor.x', 0.5);
        this.item_b.setAll('anchor.y', 0.5);
        this.item_b.setAll('checkWorldBounds', true);
        this.item_b.setAll('outOfBoundsKill', true);
        this.item_b.forEach( function(item) {
            item.animations.add('ani', [0, 1, 2, 3, 4, 5], 8, true);
        });



        // explosion
        /***********************************************************************/
        this.explosions = game.add.group();
        this.explosions.enableBody = true;
        this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
        this.explosions.createMultiple(this.explosion_num, 'explosion');
        this.explosions.setAll('anchor.x', 0.5);
        this.explosions.setAll('anchor.y', 0.5);
        // this.explosion.setAll('scale.x', 1.5);
        // this.explosion.setAll('scale.y', 1.5);
        // this.explosion.setAll('angle', 180);
        // this.explosion.setAll('outOfBoundsKill', true);
        // this.explosion.setAll('checkWorldBounds', true);
        this.explosions.forEach( function(explosion) {
            explosion.animations.add('explosion', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 16, false);
        });




        // shield
        /***********************************************************************/
        this.shield = game.add.group();
        this.shield.enableBody = true;
        this.shield.physicsBodyType = Phaser.Physics.ARCADE;
        this.shield.createMultiple(this.shield_num, 'shield');
        this.shield.setAll('anchor.x', 0.5);
        this.shield.setAll('anchor.y', 0.5);



        // UI
        /***********************************************************************/
        game.global.score = 0;
        this.score_text = game.add.text(10, game.height-10, 'Score:' + game.global.score, { font: '20px Arial', fill: '#fff' });
        this.score_text.anchor.setTo(0, 1);

        this.hpbar_empty = game.add.sprite(game.width-10,  game.height-10, 'hpbar_empty');
        this.hpbar_empty.anchor.setTo(1, 1);

        this.hpbar = game.add.sprite(game.width-10-35,  game.height-37-(10+0.89), 'hpbar');
        this.hpbar.anchor.setTo(1, 0);
        this.hp_range = new Phaser.Rectangle(0, 0, this.hpbar.width, this.hpbar.height);
        this.hpbar_maxwidth = this.hpbar.width;
        this.hpbar.crop(this.hp_range);


        this.hpbar_empty2 = game.add.sprite(game.width-10,  game.height-70, 'hpbar_empty');
        this.hpbar_empty2.anchor.setTo(1, 1);

        this.hpbar2 = game.add.sprite(game.width-10-35,  game.height-37-(70+0.89), 'hpbar');
        this.hpbar2.anchor.setTo(1, 0);
        this.hp_range2 = new Phaser.Rectangle(0, 0, this.hpbar2.width, this.hpbar2.height);
        // this.hpbar_maxwidth = this.hpbar.width;
        this.hpbar2.crop(this.hp_range2);



        this.pauseButton = game.add.button(game.width - 10, 10, 'pause_img', this.pause, this);
        this.pauseButton.anchor.setTo(1, 0);
        this.pauseButton.scale.setTo(0.8, 0.8);

        this.pause_text = game.add.text(game.width/2, game.height/2-100, 'Game Pause', { font: '50px Arial', fill: '#fff' });
        this.pause_text.anchor.setTo(0.5, 0.5);
        this.pause_text.alpha = 0;

        

        this.power_1 = game.add.sprite(game.width/2 - 50 -20, game.height-20, 'power');
        this.power_1.scale.setTo(0.15, 0.15);
        this.power_1.anchor.setTo(0.5, 0.5);

        this.power_2 = game.add.sprite(game.width/2-20, game.height-20, 'power');
        this.power_2.scale.setTo(0.15, 0.15);
        this.power_2.anchor.setTo(0.5, 0.5);

        this.power_3 = game.add.sprite(game.width/2 + 50-20, game.height-20, 'power');
        this.power_3.scale.setTo(0.15, 0.15);
        this.power_3.anchor.setTo(0.5, 0.5);



        /***********************************************************************/

        this.totalscore = 0;
        this.win = false;

        /***********************************************************************/



        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.fireButton2 = this.input.keyboard.addKey(Phaser.KeyCode.F);
        // this.lighteningButton = this.input.keyboard.addKey(Phaser.KeyCode.M);
        this.powerButton = this.input.keyboard.addKey(Phaser.KeyCode.M);


        this.scoreupButton = this.input.keyboard.addKey(Phaser.KeyCode.P);
        this.killButton = this.input.keyboard.addKey(Phaser.KeyCode.O);


        this.cursor2_up = this.input.keyboard.addKey(Phaser.KeyCode.W);
        this.cursor2_down = this.input.keyboard.addKey(Phaser.KeyCode.S);
        this.cursor2_left = this.input.keyboard.addKey(Phaser.KeyCode.A);
        this.cursor2_right = this.input.keyboard.addKey(Phaser.KeyCode.D);

        this.cursor = game.input.keyboard.createCursorKeys();


        /***********************************************************************/
        // console.log(game.global.menubgm.isPlaying);
        if(game.global.menubgm.isPlaying){
            game.global.menubgm.stop();
        }
        this.boss_show_sound = game.add.audio('boss_show');
        this.boss_show_sound.volume = game.global.bgmvolume+0.7;

        this.bullet_red_sound = game.add.audio('bullet_red_sound');
        this.bullet_red_sound.volume = game.global.effectvolume;

        this.s_explode_sound = game.add.audio('s_explode_sound');
        this.s_explode_sound.volume = game.global.effectvolume;

        this.b_explode_sound = game.add.audio('b_explode_sound');
        this.b_explode_sound.volume = game.global.effectvolume;

        this.bullet_auto_sound = game.add.audio('bullet_auto_sound');
        this.bullet_auto_sound.volume = game.global.effectvolume;


        
        game.global.prev_play_state = 'game_2p_H';
    },

    update : function(){
        if(game.global.menubgm.isPlaying){
            // console.log('a');
            game.global.menubgm.stop();
            // console.log('b');
        }

        if(!this.IslaunchBoss){
            if(!game.global.gamebgm.isPlaying){
                game.global.gamebgm.play();
            }
            if(game.global.bossbgm.isPlaying){
                game.global.bossbgm.stop();
            }
        }else{
            if(!game.global.bossbgm.isPlaying){
                game.global.bossbgm.play();
            }
            if(game.global.gamebgm.isPlaying){
                game.global.gamebgm.stop();
            }
        }
        
        



        game.physics.arcade.overlap(this.enemies_1, this.bullet_red.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_l.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_l.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_l.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_r.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_r.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_r.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_help.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_help.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_help.bullets, this.hitBoss, null, this);

        

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red2.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red2.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red2.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_l2.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_l2.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_l2.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_r2.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_r2.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_r2.bullets, this.hitBoss, null, this);

        game.physics.arcade.overlap(this.enemies_1, this.bullet_red_help2.bullets, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_red_help2.bullets, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_red_help2.bullets, this.hitBoss, null, this);



        game.physics.arcade.overlap(this.enemies_1, this.bullet_auto, this.hitEnemies_1, null, this);
        game.physics.arcade.overlap(this.enemies_2, this.bullet_auto, this.hitEnemies_2, null, this);
        game.physics.arcade.overlap(this.boss, this.bullet_auto, this.hitBoss, null, this);

        // game.physics.arcade.overlap(this.enemies_1, this.lightening, this.hitlightening_1, null, this, 1);
        // game.physics.arcade.overlap(this.enemies_2, this.lightening, this.hitlightening_2, null, this, 2);
        // game.physics.arcade.overlap(this.boss, this.lightening, this.hitlightening_boss, null, this, 3);
        game.physics.arcade.overlap(this.player, this.bullet_enemy, this.hitPlayer, null, this);

        game.physics.arcade.overlap(this.player, this.item_r, this.getitem_r, null, this);
        game.physics.arcade.overlap(this.player, this.item_g, this.getitem_g, null, this);
        game.physics.arcade.overlap(this.player, this.item_b, this.getitem_b, null, this);

        game.physics.arcade.overlap(this.player2, this.bullet_enemy, this.hitPlayer, null, this);

        game.physics.arcade.overlap(this.player2, this.item_r, this.getitem_r, null, this);
        game.physics.arcade.overlap(this.player2, this.item_g, this.getitem_g, null, this);
        game.physics.arcade.overlap(this.player2, this.item_b, this.getitem_b, null, this);



        this.bg1.tilePosition.y += 0.15;
        this.bg2.tilePosition.y += 0.3;
        this.bg3.tilePosition.y += 0.5;

        this.Player_action();



        this.bullet_auto.forEachAlive(function (bullet) {
            if(bullet.target){
                if(bullet.target.exists){
                    game.physics.arcade.moveToObject(bullet, bullet.target, 200);
                } else {
                    var e1 = this.enemies_1.getFirstExists(true);
                    if(e1){
                        bullet.target = e1;
                    } else {
                        var e2 = this.enemies_2.getFirstExists(true);
                        if(e2){
                            bullet.target = e2;
                        } else {
                            if(this.boss.exists){
                                bullet.target = this.boss;
                            } else {
                                bullet.body.velocity.y = -200;
                            }
                        }
                    }
                }
            } else {
                bullet.body.velocity.y = -200;
            }
        }, this);


        this.enemy_fire();
        this.score_update();
        this.item_create();

        this.Boss_update();

        if(this.helper.exists && this.player.alive){
            this.bullet_red_help.fire();
            this.bullet_red_sound.play();
        }

        if(this.helper2.exists && this.player2.alive){
            this.bullet_red_help2.fire();
            this.bullet_red_sound.play();
        }


        this.cheat();

    },



    /***********************************************************************/

    cheat : function() {
        if(this.scoreupButton.isDown){
            this.totalscore += 100;
            game.global.score += 100;
            // console.log('cheat ' + this.totalscore);
        } else if(this.killButton.isDown){
            this.player.hp -= 99;
            this.player2.hp -= 99;
        }
    },


    Player_action : function() {
        this.movePlayer();
        this.fire();
    },

    movePlayer : function(){
        if(this.cursor.left.isDown) {
            if(this.cursor.right.isDown) {
                this.player.body.velocity.x = 0;

                if(this.player.frame > 5) {
                    this.player.frame--;
                } else if(this.player.frame < 5) {
                    this.player.frame++;
                }

            } else {
                this.player.body.velocity.x = -200;
                
                if(this.player.frame != 0) {
                    this.player.frame--;
                }
            }
            
        } else if(this.cursor.right.isDown) {
            this.player.body.velocity.x = 200;

            if(this.player.frame != 10) {
                this.player.frame++;
            }

        } else {
            this.player.body.velocity.x = 0;

            if(this.player.frame > 5) {
                this.player.frame--;
            } else if(this.player.frame < 5) {
                this.player.frame++;
            }
        }


        if(this.cursor.up.isDown) {
            this.player.body.velocity.y = -350;
            
        } else if(this.cursor.down.isDown) {
            this.player.body.velocity.y = 350;

        } else {
            this.player.body.velocity.y = 0;
        }








        if(this.cursor2_left.isDown) {
            if(this.cursor.right.isDown) {
                this.player2.body.velocity.x = 0;

                if(this.player2.frame > 5) {
                    this.player2.frame--;
                } else if(this.player2.frame < 5) {
                    this.player2.frame++;
                }

            } else {
                this.player2.body.velocity.x = -200;
                
                if(this.player2.frame != 0) {
                    this.player2.frame--;
                }
            }
            
        } else if(this.cursor2_right.isDown) {
            this.player2.body.velocity.x = 200;

            if(this.player2.frame != 10) {
                this.player2.frame++;
            }

        } else {
            this.player2.body.velocity.x = 0;

            if(this.player2.frame > 5) {
                this.player2.frame--;
            } else if(this.player2.frame < 5) {
                this.player2.frame++;
            }
        }


        if(this.cursor2_up.isDown) {
            this.player2.body.velocity.y = -350;
            
        } else if(this.cursor2_down.isDown) {
            this.player2.body.velocity.y = 350;

        } else {
            this.player2.body.velocity.y = 0;
        }
    },



    
    fire : function(){
        if (this.fireButton.isDown && this.player.alive){
            if(this.item_ring){
                this.bullet_red_l.fire();
                this.bullet_red_r.fire();
                this.bullet_red.fire();
                // this.bullet_red_sound.play();
            } 
            if(this.item_bing){

                if(game.time.now > this.bullet_auto_firingdelay + this.bullet_auto.lastShot){
                    this.bullet_auto.lastShot = game.time.now;

                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player.x, this.player.y - 20);
                        bullet.damage = 5;

                        // bullet.body.velocity.y = this.enemies_1_bulletSpeed;



                        bullet.scale.setTo(1, 1);

                        var e1 = this.enemies_1.getFirstExists(true);
                        if(e1){
                            bullet.target = e1;
                        } else {
                            bullet.target = null;
                        }
                        

                        this.bullet_auto_sound.play();
                    }
                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player.x, this.player.y - 20);
                        bullet.damage = 5;

                        // bullet.body.velocity.y = this.enemies_1_bulletSpeed;



                        bullet.scale.setTo(1, 1);

                        var e2 = this.enemies_2.getFirstExists(true);
                        if(e2){
                            bullet.target = e2;
                        } else {
                            bullet.target = null;
                        }

                        this.bullet_auto_sound.play();
                    }
                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player.x, this.player.y - 20);
                        bullet.damage = 5;

                        // 

                        bullet.scale.setTo(1, 1);

                        if(this.boss.exists){
                            bullet.target = this.boss;
                        } else {
                            bullet.target = null;
                        }

                        this.bullet_auto_sound.play();
                    }
                    
                }
                
            }
            this.bullet_red.fire();
            this.bullet_red_sound.play();

        }





        if (this.fireButton2.isDown && this.player2.alive){
            if(this.item_ring){
                this.bullet_red_l2.fire();
                this.bullet_red_r2.fire();
                this.bullet_red2.fire();
                // this.bullet_red_sound.play();
            } 
            if(this.item_bing){

                if(game.time.now > this.bullet_auto_firingdelay + this.bullet_auto.lastShot){
                    this.bullet_auto.lastShot = game.time.now;

                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player2.x, this.player2.y - 20);
                        bullet.damage = 5;

                        // bullet.body.velocity.y = this.enemies_1_bulletSpeed;



                        bullet.scale.setTo(1, 1);

                        var e1 = this.enemies_1.getFirstExists(true);
                        if(e1){
                            bullet.target = e1;
                        } else {
                            bullet.target = null;
                        }
                        

                        this.bullet_auto_sound.play();
                    }
                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player2.x, this.player2.y - 20);
                        bullet.damage = 5;

                        // bullet.body.velocity.y = this.enemies_1_bulletSpeed;



                        bullet.scale.setTo(1, 1);

                        var e2 = this.enemies_2.getFirstExists(true);
                        if(e2){
                            bullet.target = e2;
                        } else {
                            bullet.target = null;
                        }

                        this.bullet_auto_sound.play();
                    }
                    var bullet = this.bullet_auto.getFirstExists(false);
                    if(bullet){
                        
                        bullet.reset(this.player2.x, this.player2.y - 20);
                        bullet.damage = 5;

                        // 

                        bullet.scale.setTo(1, 1);

                        if(this.boss.exists){
                            bullet.target = this.boss;
                        } else {
                            bullet.target = null;
                        }

                        this.bullet_auto_sound.play();
                    }
                    
                }
                
            }
            this.bullet_red2.fire();
            this.bullet_red_sound.play();

        }

        // if (this.lighteningButton.isDown && this.player.alive){
        //     this.lightening.reset(this.player.x, this.player.y-20);
        //     this.lightening.play('anim');
        //     game.time.events.add(this.lightening_keeptime, this.lightening_end, this);
        // }

        if (!this.powering && this.power_number>0 && this.powerButton.isDown && (this.player.alive || this.player2.alive)){

            if(this.power_number == 3){
                this.power_3.kill();
            } else if(this.power_number == 2){
                this.power_2.kill();
            } else if(this.power_number == 1){
                this.power_1.kill();
            }


            this.power_number--;
            this.powering = true;

            

            game.time.events.add(2000, this.power_end, this);

            game.camera.flash(0xFFFFFF , 500);
            game.camera.shake(0.005, 500);


            this.enemies_1.forEachAlive(function (enemy) {
                var explosion = this.explosions.getFirstExists(false);
                if(explosion){
                    explosion.reset(enemy.x, enemy.y);
                    explosion.body.velocity.y = enemy.body.velocity.y;
                    explosion.scale.setTo(0.7, 0.7);
                    explosion.alpha = 0.7;
                    explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
                }
                
                enemy.kill();
                game.global.score += enemy.score;
            }, this);

            this.enemies_2.forEachAlive(function (enemy) {
                var explosion = this.explosions.getFirstExists(false);
                if(explosion){
                    explosion.reset(enemy.x, enemy.y);
                    explosion.body.velocity.y = enemy.body.velocity.y;
                    explosion.scale.setTo(1.4, 1.4);
                    explosion.alpha = 0.7;
                    explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
                }
                
                enemy.kill();
                game.global.score += enemy.score;
            }, this);
        }

    },

    power_end : function(){
        this.powering = false;
    },


    item_create : function(){
        if(game.global.score - this.item_score_acc >= this.item_score){
            this.item_score_acc += this.item_score;

            var choose = game.rnd.integerInRange(1,3);

            if(choose == 1){
                item = this.item_r.getFirstExists(false);
                if(item){
                    item.reset(game.rnd.integerInRange(50,game.width-50), -10);
                    item.body.velocity.y = 60;

                    item.scale.setTo(0.15, 0.15);

                    item.play('ani');

                }
            } else if(choose == 2){
                item = this.item_g.getFirstExists(false);
                if(item){
                    item.reset(game.rnd.integerInRange(50,game.width-50), -10);
                    item.body.velocity.y = 60;

                    item.scale.setTo(0.15, 0.15);

                    item.play('ani');


                }
            } else if(choose == 3){
                item = this.item_b.getFirstExists(false);
                if(item){
                    item.reset(game.rnd.integerInRange(50,game.width-50), -10);
                    item.body.velocity.y = 60;

                    item.scale.setTo(0.15, 0.15);

                    item.play('ani');


                }
            }

        }
    },

    getitem_r : function(player, item){
        this.item_ring = true;
        item.kill();
        game.time.events.add(8000, this.item_r_end, this);
    },

    getitem_g : function(player, item){
        this.item_ging = true;

        this.helper.exists = true;
        this.helper.reset(-100,50);
        this.player.addChild(this.helper);

        this.helper2.exists = true;
        this.helper2.reset(-100,50);
        this.player2.addChild(this.helper2);

        item.kill();
        game.time.events.add(8000, this.item_g_end, this);
    },

    getitem_b : function(player, item){
        this.item_bing = true;
        item.kill();
        game.time.events.add(8000, this.item_b_end, this);
    },


    item_r_end : function(){
        this.item_ring = false;
    },

    item_g_end : function(){
        this.item_ging = false;
        this.helper.exists = false;
    },

    item_b_end : function(){
        this.item_bing = false;
    },


    // lightening_end : function(){
    //     this.lightening.exists = false;
    // },


    // hitlightening_1 : function(enemy, lightening){

    //     // console.log(game.time.now + ' ' + enemy.last_hitbylight +' '+ this.lightening_hitdelay +' ' );

    //     // if(game.time.now > enemy.last_hitbylight + this.lightening_hitdelay){
    //         // enemy.last_hitbylight = game.time.now;

    //         console.log(enemy.hp);
    //         enemy.hp -= lightening.damage;
    //         if(enemy.hp <= 0){

    //             var explosion = this.explosions.getFirstExists(false);
    //             if(explosion){
    //                 explosion.reset(enemy.x, enemy.y);
    //                 explosion.body.velocity.y = enemy.body.velocity.y;

    //                 explosion.scale.setTo(0.7, 0.7);

    //                 explosion.alpha = 0.7;
    //                 explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
    //             }

    //             enemy.kill();

    //             game.global.score += enemy.score;
    //         }

    //     // }
    // },

    // hitlightening_2 : function(enemy, lightening){
    //     // if(game.time.now > enemy.last_hitbylight + this.lightening_hitdelay){
    //         // enemy.last_hitbylight = game.time.now;
    //         console.log('a');

    //         var theshield = this.shield.getFirstExists(false);
    //         if (theshield) {
    //             theshield.reset(0, 0);

    //             theshield.scale.setTo(0.33, 0.35);
    //             theshield.alpha = 1;
                
                
                
    //             enemy.addChild(theshield);

    //             game.time.events.add(0.3, this.shiled_dispear, this, theshield);
    //         }
            
            


    //         enemy.hp -= lightening.damage;
    //         if(enemy.hp <= 0){

    //             var explosion = this.explosions.getFirstExists(false);
    //             if(explosion){
    //                 explosion.reset(enemy.x, enemy.y);
    //                 explosion.body.velocity.y = enemy.body.velocity.y;

    //                 explosion.scale.setTo(1.4, 1.4);
                    
    //                 explosion.alpha = 0.7;
    //                 explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
    //             }

    //             enemy.kill();

    //             game.global.score += enemy.score;
    //         }

    //     // }
    // },

    // hitlightening_boss : function(enemy, lightening){
    //     if(game.time.now > enemy.last_hitbylight + this.lightening_hitdelay){
    //         enemy.last_hitbylight = game.time.now;

    //         var theshield = this.shield.getFirstExists(false);
    //         if (theshield) {
    //             theshield.reset(0, 0);

    //             if(this.boss.frame == 4){
    //                 theshield.scale.setTo(1.2, 1.2);
    //             } else if(this.boss.frame == 3){
    //                 theshield.scale.setTo(1.5, 1.2);
    //             } else {
    //                 theshield.scale.setTo(0.9, 1.2);
    //             }
    //             theshield.alpha = 0.3;
                
                
                
    //             enemy.addChild(theshield);

    //             game.time.events.add(0.3, this.shiled_dispear, this, theshield);
    //         }
            
            
    //         enemy.hp -= lightening.damage;
    //         if(enemy.hp <= 0){

    //             // game.global.score += boss.score;
    //             enemy.state = 0;
                
    //             game.global.score += enemy.score;
    //         }

    //     }
    // },


    hitPlayer : function(player, bullet) {

        if(game.time.now > player.lastgethit + this.player_Invincible_delay){

            player.lastgethit = game.time.now;

            var theshield = this.shield.getFirstExists(false);
            if (theshield) {
                theshield.reset(0, 5);
                theshield.scale.setTo(0.3, 0.3);
                player.addChild(theshield);

                
                game.time.events.add(this.player_Invincible_delay, this.shiled_dispear, this, theshield);
            }


            player.hp -= bullet.damage;

            this.hp_range.width = (this.player.hp / this.player_hp) * this.hpbar_maxwidth;
            this.hpbar.updateCrop();

            this.hp_range2.width = (this.player2.hp / this.player_hp) * this.hpbar_maxwidth;
            this.hpbar2.updateCrop();

            if(player.hp <= 0){
                for(var i=0; i<5; i++){
                    var explosion = this.explosions.getFirstExists(false);
                    if(explosion){
                        explosion.reset(player.x + game.rnd.integerInRange(-40, 40), player.y + game.rnd.integerInRange(-40, 40));
                        explosion.body.velocity.y = player.body.velocity.y;
                        var tmp = 0.1*game.rnd.integerInRange(-2, 3);
                        explosion.scale.setTo(1 + tmp, 1 + tmp);
                        explosion.alpha = 0.7;
                        explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
                    }
                    this.s_explode_sound.play();
                }
                
                
        
                player.kill();
        
                if(!this.player.alive && !this.player2.alive){
                    game.camera.onFadeComplete.add(this.gameover, this);
                    game.camera.fade("rgb(0, 0, 0)", 2000);
                }
                
            }
        }
        

        bullet.kill();
    },



    gameover : function() {
        game.state.start('gameover');
    },

    wingame : function() {
        // this.boss.kill();
        game.camera.onFadeComplete.add(this.winend, this);
        game.camera.fade("rgb(0, 0, 0)", 2000);

        // game.state.start('gameover');
    },

    winend : function() {
        game.state.start('gamewin');
    },


    pause : function() {
        game.paused = !game.paused;
        if(game.paused){
            this.pause_text.alpha = 1;
        } else {
            this.pause_text.alpha = 0;
        }
        
    },



    score_update : function() {
        this.score_text.text = 'Score: ' + game.global.score;
    },



    /*************************/
    
    launchEnemy : function(type) {
        var MIN_ENEMY_SPACING;
        var MAX_ENEMY_SPACING;
        // var choose = game.rnd.integerInRange(1, 8);
        var enemy;

        if(type == 1){
            // console.log('1');
            MIN_ENEMY_SPACING = this.enemies_1_MINSPACING;
            MAX_ENEMY_SPACING = this.enemies_1_MAXSPACING;
            enemy = this.enemies_1.getFirstExists(false);
            if (enemy) {
                enemy.reset(game.rnd.integerInRange(100, game.width-100), -10);
                // enemy.body.velocity.x = game.rnd.integerInRange(-100, 100);
                enemy.body.velocity.y = this.enemies_1_speed;
                // enemy.body.drag.x = this.enemies_2_turnspeed;
                enemy.body.acceleration.x = 0;

                enemy.hp = this.enemies_1_hp;

                enemy.firerate = this.enemies_1_firerate;
                enemy.score = this.enemies_1_score;

                enemy.update = function(){
                    enemy.body.acceleration.x += game.rnd.integerInRange(-10, 10);
                }
            }
        } else if(type == 2){
            MIN_ENEMY_SPACING = this.enemies_2_MINSPACING;
            MAX_ENEMY_SPACING = this.enemies_2_MAXSPACING;
            if(this.totalscore >= 100){
                enemy = this.enemies_2.getFirstExists(false);
                if (enemy) {
                    enemy.reset(game.rnd.integerInRange(50, game.width-50), -50);
                    enemy.body.velocity.x = game.rnd.integerInRange(-100, 100);
                    enemy.body.velocity.y = this.enemies_2_speed;
                    enemy.body.drag.x = this.enemies_2_turnspeed;


                    enemy.hp = this.enemies_2_hp;

                    enemy.firerate = this.enemies_2_firerate;
                    enemy.score = this.enemies_2_score;


                    enemy.update = function(){
                        enemy.angle = 180 - game.math.radToDeg(Math.atan2(enemy.body.velocity.x, enemy.body.velocity.y));
                    }
                }
            }
        }
        
        if(enemy){

            enemy.lastShot = game.time.now;
            // enemy.last_hitbylight = game.time.now - this.lightening_hitdelay;
            

            this.totalscore += enemy.score;
        }

        if(!this.IslaunchBoss){
            //  Send another enemy soon
            game.time.events.add(game.rnd.integerInRange(MIN_ENEMY_SPACING, MAX_ENEMY_SPACING), this.launchEnemy, this, type);
            if(this.totalscore >= this.totalscore_launchboss) {
                this.launchBoss();
            }
        }
        
    },


    launchBoss : function() {
        this.IslaunchBoss = true;

        this.boss.exists = true;

        this.boss.lastShot_A = game.time.now;
        this.boss.lastShot_B = game.time.now;
        this.boss.lastShot_C = game.time.now;
        this.boss.lasttype_C = game.time.now;
        this.boss.lasttypeStart_C = game.time.now;

        // this.boss.last_hitbylight = game.time.now - this.lightening_hitdelay;
        this.boss_show_sound.play();

    },

    Boss_update : function() {
        if(this.boss.exists){

            if(this.boss.state == 1){
                if(this.boss.y < this.boss.height/2 + 10){
                    this.boss.body.velocity.y = 80;
                } else if(this.boss.y < this.boss.height/2 + 20){
                    this.boss.body.velocity.x = 0;
                    this.boss.invincible = false;
                } else {
                    this.boss.body.velocity.y = 0;

                    if(this.boss.x < 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(1, 5);
                    } else if(this.boss.x > game.width - 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(-5, -1);
                    } else {
                        this.boss.body.velocity.x += game.rnd.integerInRange(-10, 10);
                    }



                    // bullet A
                    /***********************************************************************/
                    if(game.time.now > this.boss_firingdelay_A + this.boss.lastShot_A){
                        this.boss.lastShot_A = game.time.now;
                        this.boss_turnangleNOW_A += this.boss_turnangle_A;

                        var bullet = this.bullet_enemy.getFirstExists(false);
                        if(bullet){
                            this.boss.lastShot_A = game.time.now;
                            bullet.reset(this.boss.x - 45, this.boss.y + 113);
                            bullet.damage = this.boss_damage_A;
                            bullet.scale.setTo(1, 1);
                            game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_A);
                        }
                        var bullet = this.bullet_enemy.getFirstExists(false);
                        if(bullet){
                            this.boss.lastShot_A = game.time.now;
                            bullet.reset(this.boss.x + 45, this.boss.y + 113);
                            bullet.damage = this.boss_damage_A;
                            bullet.scale.setTo(1, 1);
                            game.physics.arcade.moveToObject(bullet, this.player2, this.boss_bulletSpeed_A);
                        }
                        var bullet = this.bullet_enemy.getFirstExists(false);
                        if(bullet){
                            this.boss.lastShot_A = game.time.now;
                            bullet.reset(this.boss.x + 66, this.boss.y + 80);
                            bullet.damage = this.boss_damage_A;
                            bullet.scale.setTo(0.7, 0.7);
                            game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_A - 30);
                        }
                        var bullet = this.bullet_enemy.getFirstExists(false);
                        if(bullet){
                            this.boss.lastShot_A = game.time.now;
                            bullet.reset(this.boss.x - 66, this.boss.y + 80);
                            bullet.damage = this.boss_damage_A;
                            bullet.scale.setTo(0.7, 0.7);
                            game.physics.arcade.moveToObject(bullet, this.player2, this.boss_bulletSpeed_A - 30);
                        }
                    }



                }
            } else if(this.boss.state == 2){
                if(this.boss.y < this.boss.height/2 + 10){
                    this.boss.body.velocity.y = 80;
                } else if(this.boss.y < this.boss.height/2 + 20){
                    this.boss.body.velocity.x = 0;
                    this.boss.invincible = false;
                } else {
                    this.boss.body.velocity.y = 0;
                    
                    if(this.boss.x < 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(1, 5);
                    } else if(this.boss.x > game.width - 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(-5, -1);
                    } else {
                        this.boss.body.velocity.x += game.rnd.integerInRange(-10, 10);
                    }


                    if(!this.boss.C){
                        if(game.time.now > this.boss_typedelay_C + this.boss.lasttype_C){
                            this.boss.C = true;
                            this.boss.lasttypeStart_C = game.time.now;
                            this.boss.frame = 2;
                            // game.physics.arcade.enable(this.boss);
                            // this.boss.reset(this.boss.x, this.boss.y);
                            this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                        }

                        // bullet A
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_A + this.boss.lastShot_A){
                            this.boss.lastShot_A = game.time.now;
                            // this.boss_turnangleNOW_A += this.boss_turnangle_A;

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_A = game.time.now;
                                bullet.reset(this.boss.x - 55, this.boss.y + 113);
                                bullet.damage = this.boss_damage_A;
                                bullet.scale.setTo(1, 1);
                                game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_A - 40);
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_A = game.time.now;
                                bullet.reset(this.boss.x + 55, this.boss.y + 113);
                                bullet.damage = this.boss_damage_A;
                                bullet.scale.setTo(1, 1);
                                game.physics.arcade.moveToObject(bullet, this.player2, this.boss_bulletSpeed_A - 40);
                            }
                        }

                        // bullet B
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_B + this.boss.lastShot_B){
                            this.boss.lastShot_B = game.time.now;
                            this.boss_turnangleNOW_B += this.boss_turnangle_B;

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x - 68, this.boss.y + 72);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x + 68, this.boss.y + 72);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x - 68, this.boss.y + 72);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = -v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x + 68, this.boss.y + 72);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = -v.y;
                            }
                            
                        }
                    } else {
                        if(game.time.now > this.boss_typekeep_C + this.boss.lasttypeStart_C){
                            this.boss.C = false;
                            this.boss.lasttype_C = game.time.now;
                            this.boss.frame = 1;
                            // game.physics.arcade.enable(this.boss);
                            // this.boss.reset(this.boss.x, this.boss.y);
                            this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                            this.boss_turnangleNOW_C = 0;
                            // console.log(this.boss.width + ' ' + this.boss.body.width);
                        }
                        
                            // bullet C
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_C + this.boss.lastShot_C){
                            this.boss.lastShot_C = game.time.now;
                            this.boss_turnangleNOW_C += this.boss_turnangle_C;

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x - 68, this.boss.y + 120);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x + 68, this.boss.y + 120);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x - 80, this.boss.y + 25);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x + 80, this.boss.y + 25);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            
                        }
                    }


                }
            } else if(this.boss.state == 3){
                if(this.boss.y < this.boss.height/2 + 10){
                    this.boss.body.velocity.y = 80;
                } else if(this.boss.y < this.boss.height/2 + 20){
                    this.boss.body.velocity.x = 0;
                    this.boss.invincible = false;
                } else {
                    this.boss.body.velocity.y = 0;
                    
                    if(this.boss.x < 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(1, 5);
                    } else if(this.boss.x > game.width - 150){
                        this.boss.body.velocity.x += game.rnd.integerInRange(-5, -1);
                    } else {
                        this.boss.body.velocity.x += game.rnd.integerInRange(-10, 10);
                    }



                    if(!this.boss.C){
                        if(game.time.now > this.boss_typedelay_C + this.boss.lasttype_C){
                            this.boss.C = true;
                            this.boss.lasttypeStart_C = game.time.now;
                            this.boss.frame = 3;
                            // game.physics.arcade.enable(this.boss);
                            // this.boss.reset(this.boss.x, this.boss.y);
                            this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                        }

                        // bullet A
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_A + this.boss.lastShot_A){
                            this.boss.lastShot_A = game.time.now;
                            // this.boss_turnangleNOW_A += this.boss_turnangle_A;

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_A = game.time.now;
                                bullet.reset(this.boss.x - 55, this.boss.y + 113);
                                bullet.damage = this.boss_damage_A;
                                bullet.scale.setTo(1.3, 1.3);
                                game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_A - 60);
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_A = game.time.now;
                                bullet.reset(this.boss.x + 55, this.boss.y + 113);
                                bullet.damage = this.boss_damage_A;
                                bullet.scale.setTo(1.3, 1.3);
                                game.physics.arcade.moveToObject(bullet, this.player2, this.boss_bulletSpeed_A - 60);
                            }
                        }

                        // bullet B
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_B + this.boss.lastShot_B){
                            this.boss.lastShot_B = game.time.now;

                            this.boss_turnangleNOW_B += (this.boss_turnangle_B-5);
                            

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x - 80, this.boss.y + 45);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x + 80, this.boss.y + 45);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x - 100, this.boss.y + 0);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B-10, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_B = game.time.now;
                                bullet.reset(this.boss.x + 100, this.boss.y + 0);
                                bullet.damage = this.boss_damage_B;
                                bullet.scale.setTo(0.5, 0.5);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_B);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_B-10, this.boss_bulletSpeed_B);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            
                        }
                    } else {
                        if(game.time.now > this.boss_typekeep_C + this.boss.lasttypeStart_C){
                            this.boss.C = false;
                            this.boss.lasttype_C = game.time.now;
                            this.boss.frame = 4;
                            // game.physics.arcade.enable(this.boss);
                            // this.boss.reset(this.boss.x, this.boss.y);
                            this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                            this.boss_turnangleNOW_C = 0;
                        }
                        
                            // bullet C
                        /***********************************************************************/
                        if(game.time.now > this.boss_firingdelay_C + this.boss.lastShot_C){
                            this.boss.lastShot_C = game.time.now;
                            this.boss_turnangleNOW_C += this.boss_turnangle_C;

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x - 68, this.boss.y + 120);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x + 68, this.boss.y + 120);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x - 115, this.boss.y + 45);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x + 115, this.boss.y + 45);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }

                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x - 145, this.boss.y + 10);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);
                                
                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = -v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            var bullet = this.bullet_enemy.getFirstExists(false);
                            if(bullet){
                                this.boss.lastShot_C = game.time.now;
                                bullet.reset(this.boss.x + 145, this.boss.y + 10);
                                bullet.damage = this.boss_damage_C;
                                bullet.scale.setTo(0.4, 0.4);

                                // game.physics.arcade.moveToObject(bullet, this.player, this.boss_bulletSpeed_C);
                                var v = game.physics.arcade.velocityFromAngle(this.boss_turnangleNOW_C, this.boss_bulletSpeed_C);
                                bullet.body.velocity.x = v.x;
                                bullet.body.velocity.y = v.y;
                            }
                            
                        }
                    }


                }
            } else if(this.boss.state == 0){
                
                var explosion = this.explosions.getFirstExists(false);
                if(explosion){
                    explosion.reset(this.boss.x + game.rnd.integerInRange(-80, 80), this.boss.y + game.rnd.integerInRange(-150, 150));
                    explosion.body.velocity.y = this.boss.body.velocity.y;
                    var tmp = 0.1*game.rnd.integerInRange(-2, 3);
                    explosion.scale.setTo(1.4 + tmp, 1.4 + tmp);
                    explosion.alpha = 0.7;
                    explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
                }
                this.b_explode_sound.play();


                if(this.boss.next_state == 2){
                    if(this.boss.inWorld){
                        this.boss.body.velocity.y = -50;
                    } else {
                        this.boss.body.velocity.x = -this.boss.body.velocity.x;

                        this.boss.frame = 1;
                        // game.physics.arcade.enable(this.boss);
                        // this.boss.reset(this.boss.x, this.boss.y);
                        this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                        this.boss.state = this.boss.next_state;
                        this.boss.next_state += 1;

                        this.boss.hp = this.boss_hp_2;
                        this.boss.score = this.boss_score_2;

                        this.boss.invincible = true;
                    }
                } else if(this.boss.next_state == 3){
                    if(this.boss.inWorld){
                        this.boss.body.velocity.y = -50;
                    } else {
                        this.boss.body.velocity.x = -this.boss.body.velocity.x;
                        this.boss.frame = 4;
                        // game.physics.arcade.enable(this.boss);
                        // this.boss.reset(this.boss.x, this.boss.y);
                        this.boss.body.setSize(this.boss.width/ this.boss.scale.x, this.boss.height/ this.boss.scale.y, 0, 0);
                        this.boss.state = this.boss.next_state;
                        this.boss.next_state += 1;

                        this.boss.hp = this.boss_hp_3;
                        this.boss.score = this.boss_score_3;

                        this.boss.invincible = true;

                        this.boss.C = false;
                        this.boss.lasttype_C = game.time.now;
                        this.boss_turnangleNOW_C = 0;
                    }
                } else if(this.boss.next_state == 4){
                    if(!this.win){
                        this.boss.body.velocity.x = 0;
                        this.boss.body.velocity.y = 0;
                        this.win = true;
                        game.time.events.add(5000, this.wingame, this);
                    }
                    

                }
            }
        }
    },

    hitBoss : function(boss, bullet) {
        // console.log('hit');
        if(boss.state != 0 && !this.boss.invincible){
            var theshield = this.shield.getFirstExists(false);
            if (theshield) {
                theshield.reset(0, 0);
                // theshield.scale.setTo(0.43, 0.45);
                if(boss.frame == 4){
                    theshield.scale.setTo(1.2, 1.2);
                } else if(boss.frame == 3){
                    theshield.scale.setTo(1.5, 1.2);
                } else {
                    theshield.scale.setTo(0.9, 1.2);
                }
                
                theshield.alpha = 0.3;
                boss.addChild(theshield);

                game.time.events.add(0.3, this.shiled_dispear, this, theshield);
            }

            boss.particle.x = bullet.x;
            boss.particle.y = bullet.y;
            boss.particle.start(true, 800, null, 5);


            boss.hp -= bullet.damage;
            if(boss.hp <= 0){

                game.global.score += boss.score;

                boss.state = 0;
                // boss.next_state = 2;
                // boss.hp = this.boss_hp_2;

            }
        } else if(this.boss.invincible){
            var theshield = this.shield.getFirstExists(false);
            if (theshield) {
                theshield.reset(0, 0);
                // theshield.scale.setTo(0.43, 0.45);
                if(boss.frame == 4){
                    theshield.scale.setTo(1.2, 1.2);
                } else if(boss.frame == 3){
                    theshield.scale.setTo(1.5, 1.2);
                } else {
                    theshield.scale.setTo(0.9, 1.2);
                }
                
                theshield.alpha = 0.3;
                boss.addChild(theshield);

                game.time.events.add(0.3, this.shiled_dispear, this, theshield);
            }

            boss.particle.x = bullet.x;
            boss.particle.y = bullet.y;
            boss.particle.start(true, 800, null, 5);
        } else {
            boss.particle.x = bullet.x;
            boss.particle.y = bullet.y;
            boss.particle.start(true, 800, null, 5);

            game.global.score += 1;
        }
        


        bullet.kill();
    },


    hitEnemies_1 : function(enemy, bullet) {

        enemy.particle.x = bullet.x;
        enemy.particle.y = bullet.y;
        enemy.particle.start(true, 800, null, 5);


        enemy.hp -= bullet.damage;
        if(enemy.hp <= 0){
            var explosion = this.explosions.getFirstExists(false);
            if(explosion){
                explosion.reset(enemy.x, enemy.y);
                explosion.body.velocity.y = enemy.body.velocity.y;
                explosion.scale.setTo(0.7, 0.7);
                explosion.alpha = 0.7;
                explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
            }
            this.s_explode_sound.play();


            enemy.kill();
            game.global.score += enemy.score;
        }

        bullet.kill();
    },


    hitEnemies_2 : function(enemy, bullet) {

        var theshield = this.shield.getFirstExists(false);
        if (theshield) {
            theshield.reset(0, 0);
            // theshield.scale.setTo(0.43, 0.45);
            theshield.scale.setTo(0.33, 0.35);
            theshield.alpha = 1;
            enemy.addChild(theshield);

            game.time.events.add(0.3, this.shiled_dispear, this, theshield);
        }



        enemy.particle.x = bullet.x;
        enemy.particle.y = bullet.y;
        enemy.particle.start(true, 800, null, 5);


        enemy.hp -= bullet.damage;
        if(enemy.hp <= 0){
            var explosion = this.explosions.getFirstExists(false);
            if(explosion){
                explosion.reset(enemy.x, enemy.y);
                explosion.body.velocity.y = enemy.body.velocity.y;
                explosion.scale.setTo(1.4, 1.4);
                explosion.alpha = 0.7;
                explosion.play('explosion').onComplete.add(this.explosion_end, this, explosion);
            }
            this.s_explode_sound.play();
            


            enemy.kill();
            game.global.score += enemy.score;
        }

        bullet.kill();
    },

    shiled_dispear : function(theshield) {
        this.shield.addChild(theshield);
        theshield.kill();
    },


    explosion_end: function(theexplosion) {
        theexplosion.kill();
    },


    enemy_fire : function() {
        this.enemy_1_fire();
        this.enemy_2_fire();
    },

    enemy_1_fire : function() {
        this.enemies_1.forEachAlive(function (enemy) {
            var bullet = this.bullet_enemy.getFirstExists(false);
            if(bullet && game.time.now > this.enemies_1_firingdelay + enemy.lastShot){
                enemy.lastShot = game.time.now;
                bullet.reset(enemy.x, enemy.y + 30);
                bullet.damage = this.enemies_1_damage;
                bullet.scale.setTo(0.5, 0.5);
                bullet.body.velocity.y = this.enemies_1_bulletSpeed;

                // var angle = game.physics.arcade.moveToObject(bullet, this.player, this.enemies_1_bulletSpeed);
                // angle = game.math.radToDeg(angle);
                // if(angle <= 0){ // can't shoot from back
                //     bullet.kill();
                // }
                
            }
        }, this);
        
    },

    enemy_2_fire : function() {
        this.enemies_2.forEachAlive(function (enemy) {
            var bullet = this.bullet_enemy.getFirstExists(false);
            if(bullet && game.time.now > this.enemies_2_firingdelay + enemy.lastShot){
                enemy.lastShot = game.time.now;
                var rad = game.math.degToRad(enemy.angle-90);
                bullet.reset(enemy.x + 55*Math.cos(rad), enemy.y + 55*Math.sin(rad));
                bullet.damage = this.enemies_2_damage;
                bullet.scale.setTo(0.8, 0.8);


                var ch = game.rnd.integerInRange(1, 2);
                var angle;
                if(ch == 1){
                    angle = game.physics.arcade.moveToObject(bullet, this.player, this.enemies_2_bulletSpeed);
                }else{
                    angle = game.physics.arcade.moveToObject(bullet, this.player2, this.enemies_2_bulletSpeed);
                }
                
                angle = game.math.radToDeg(angle);
                if(angle <= 0){ // can't shoot from back
                    bullet.kill();
                }
                
            }
        }, this);
        
    },

    
}