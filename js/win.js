var winState = {
    preload : function(){


        
    },

    create : function(){
        // var test_text = game.add.text(game.width/2, 150, 'win', {font: '30px Arial', fill: '#ffffff'});
        // test_text.anchor.setTo(0.5, 0.5);



        this.win_bg = game.add.sprite(game.width/2, game.height/2, 'win_bg');
        this.win_bg.anchor.setTo(0.5, 0.5);
        // this.win_bg.scale.setTo(2.2, 2.2);




        this.text_win = game.add.text(game.width/2, game.height/2 - 120, 'You Win', {font: '50px Arial', fill: '#ffffff'});
        this.text_win.anchor.setTo(0.5, 0.5);

        this.text_score = game.add.text(game.width/2, game.height/2 - 50, 'Score: ' + game.global.score, {font: '30px Arial', fill: '#ffffff'});
        this.text_score.anchor.setTo(0.5, 0.5);





        this.btn_play_pos = {x: game.width/4, y: game.height-50};
        this.btn_play = game.add.button(this.btn_play_pos.x, this.btn_play_pos.y, 'buttons', this.click_btn_play, this, 2, 0, 3);
        this.btn_play.anchor.setTo(0.5, 0.5);

        this.btn_play_text = game.add.text(this.btn_play_pos.x, this.btn_play_pos.y, 'Play Again', {font: '30px Arial', fill: '#ffffff'});
        this.btn_play_text.anchor.setTo(0.5, 0.5);


        this.btn_menu_pos = {x: game.width/4*3, y: game.height-50};
        this.btn_menu = game.add.button(this.btn_menu_pos.x, this.btn_menu_pos.y, 'buttons', this.click_btn_menu, this, 2, 0, 3);
        this.btn_menu.anchor.setTo(0.5, 0.5);
        this.btn_menu_text = game.add.text(this.btn_menu_pos.x, this.btn_menu_pos.y, 'Menu', {font: '30px Arial', fill: '#ffffff'});
        this.btn_menu_text.anchor.setTo(0.5, 0.5);



        this.btn_send_pos = {x: game.width/2, y: game.height-230};
        this.btn_send = game.add.button(this.btn_send_pos.x, this.btn_send_pos.y, 'buttons', this.click_btn_send, this, 2, 0, 3);
        this.btn_send.anchor.setTo(0.5, 0.5);
        this.btn_send_text = game.add.text(this.btn_send_pos.x, this.btn_send_pos.y, 'Send score', {font: '30px Arial', fill: '#ffffff'});
        this.btn_send_text.anchor.setTo(0.5, 0.5);
        

        this.text_Sending = game.add.text(game.width/2, game.height - 180, 'Sending', {font: '30px Arial', fill: '#ffffff'});
        this.text_Sending.anchor.setTo(0.5, 0.5);
        this.text_Sending.exists = false;
    },

    update : function(){
        if(!game.global.menubgm.isPlaying){
            game.global.menubgm.play();
        }
        if(game.global.gamebgm.isPlaying){
            game.global.gamebgm.stop();
        }
        if(game.global.bossbgm.isPlaying){
            game.global.bossbgm.stop();
        }
    },


    click_btn_send: function(){
        var input = document.getElementById('input');
        var name = input.value;
        var show = this.text_Sending;
        show.exists = true;


        var postsRef = firebase.database().ref('leaderboard');
        postsRef.push().set({
            name: name,
            score: game.global.score,
        }).then(function(){
            show.text = 'Sending complete';
        }).catch(function(e){
            console.log(e);
        });

    },



    click_btn_play : function(){

        game.state.start(game.global.prev_play_state);
    },

    click_btn_menu : function(){

        game.state.start('menu');
    },

}