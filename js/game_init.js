var game = new Phaser.Game(600, 620, Phaser.AUTO, 'canvas');




game.global = {
    score : 0,
    bgmvolume : 0.5,
    effectvolume : 0.5,
    prev_play_state : '',
    menubgm : null,
    gamebgm : null,
    bossbgm : null,
}



game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('menu_play', menuPlayState);
game.state.add('option', optionState);
game.state.add('score', scoreState);
game.state.add('game_1p', game_1pState);
game.state.add('game_1p_H', game_1p_HState);
game.state.add('gameover', gameoverState);
game.state.add('gamewin', winState);
game.state.add('level', levelState);
game.state.add('game_2p', game_2pState);
game.state.add('game_2p_H', game_2p_HState);


game.state.start('boot');