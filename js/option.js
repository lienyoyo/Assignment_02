var optionState = {
    preload : function(){


        
    },

    create : function(){

        this.option_bg = game.add.sprite(game.width/2, game.height/2, 'option_bg');
        this.option_bg.anchor.setTo(0.5, 0.5);
        



        this.title = game.add.text(game.width/2, 100, 'Option', {font: '50px Arial', fill: '#ffffff'});
        this.title.anchor.setTo(0.5, 0.5);


        var btn_back_pos = {x: game.width/2, y: game.height-120};
        var btn_back = game.add.button(btn_back_pos.x, btn_back_pos.y, 'buttons', this.click_btn_back, this, 2, 0, 3);
        btn_back.anchor.setTo(0.5, 0.5);
        var btn_back_text = game.add.text(btn_back_pos.x, btn_back_pos.y, 'Back', {font: '30px Arial', fill: '#ffffff'});
        btn_back_text.anchor.setTo(0.5, 0.5);


        this.window = game.add.sprite(game.width/2, game.height/2, 'window');
        this.window.scale.setTo(2, 2);
        this.window.anchor.setTo(0.5, 0.5);

        this.text_bgm = game.add.text(game.width/2, 235, 'BGM', {font: '30px Arial', fill: '#ffffff'});
        this.text_bgm.anchor.setTo(0.5, 0.5);

        this.btn_bgm_up = game.add.button(game.width/2+120, 280, 'speaker_up', this.click_btn_bgm_up, this);
        this.btn_bgm_up.scale.setTo(0.1, 0.1);
        this.btn_bgm_up.anchor.setTo(0.5, 0.5);

        this.btn_bgm_down = game.add.button(game.width/2-120, 280, 'speaker_down', this.click_btn_bgm_down, this);
        this.btn_bgm_down.scale.setTo(0.1, 0.1);
        this.btn_bgm_down.anchor.setTo(0.5, 0.5);

        this.text_effect = game.add.text(game.width/2, 325, 'Effect', {font: '30px Arial', fill: '#ffffff'});
        this.text_effect.anchor.setTo(0.5, 0.5);

        this.btn_effect_up = game.add.button(game.width/2+120, 370, 'speaker_up', this.click_btn_effect_up, this);
        this.btn_effect_up.scale.setTo(0.1, 0.1);
        this.btn_effect_up.anchor.setTo(0.5, 0.5);

        this.btn_effect_down = game.add.button(game.width/2-120, 370, 'speaker_down', this.click_btn_effect_down, this);
        this.btn_effect_down.scale.setTo(0.1, 0.1);
        this.btn_effect_down.anchor.setTo(0.5, 0.5);



        this.bgmbar = game.add.sprite(game.width/2-79.5,  280, 'hpbar');
        this.bgmbar.anchor.setTo(0, 0.5);
        this.bgmbar_range = new Phaser.Rectangle(0, 0, this.bgmbar.width, this.bgmbar.height);
        this.volume_bar_max =  this.bgmbar.width;
        this.bgmbar.crop(this.bgmbar_range);

        
        this.effectbar = game.add.sprite(game.width/2-79.5,  370, 'hpbar');
        this.effectbar.anchor.setTo(0, 0.5);
        this.effectbar_range = new Phaser.Rectangle(0, 0, this.effectbar.width, this.effectbar.height);
        this.effectbar.crop(this.effectbar_range);

        


        this.credit = game.add.text(game.width/2, game.height-10, 'Volume Icons made by \nhttps://www.flaticon.com/authors/smashicons', {font: '20px Arial', fill: '#ffffff'});
        this.credit.anchor.setTo(0.5, 1);
    },

    update : function(){


        this.bgmbar_range.width = (game.global.bgmvolume / 1) * this.volume_bar_max;
        this.bgmbar.updateCrop();

        this.effectbar_range.width = (game.global.effectvolume / 1) * this.volume_bar_max;
        this.effectbar.updateCrop();


    },




    click_btn_back : function(){
        game.state.start('menu');
    },


    click_btn_bgm_up : function(){
        game.global.bgmvolume += 0.1;
        if(game.global.bgmvolume <1){
            game.global.menubgm.volume = game.global.bgmvolume;
            game.global.gamebgm.volume = game.global.bgmvolume;
            game.global.bossbgm.volume = game.global.bgmvolume;
        }else {
            game.global.bgmvolume = 1;
            game.global.menubgm.volume = game.global.bgmvolume;
            game.global.gamebgm.volume = game.global.bgmvolume;
            game.global.bossbgm.volume = game.global.bgmvolume;
        }
    },

    click_btn_bgm_down : function(){
        game.global.bgmvolume -= 0.1;
        if(game.global.bgmvolume >0){
            game.global.menubgm.volume = game.global.bgmvolume;
            game.global.gamebgm.volume = game.global.bgmvolume;
            game.global.bossbgm.volume = game.global.bgmvolume;
        } else {
            game.global.bgmvolume = 0;
            game.global.menubgm.volume = game.global.bgmvolume;
            game.global.gamebgm.volume = game.global.bgmvolume;
            game.global.bossbgm.volume = game.global.bgmvolume;
        }
    },

    click_btn_effect_up : function(){
        if(game.global.effectvolume <1){
            game.global.effectvolume += 0.1;
        }
    },

    click_btn_effect_down : function(){
        if(game.global.effectvolume >0){
            game.global.effectvolume -= 0.1;
        }
    },
}