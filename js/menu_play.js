var menuPlayState = {
    preload : function(){


        
    },

    create : function(){


        this.menu_bg = game.add.sprite(game.width/2, game.height/2, 'menu_bg');
        this.menu_bg.anchor.setTo(0.5, 0.5);

        this.title = game.add.text(game.width/2, 150, 'Choose Mode', {font: '50px Arial', fill: '#ffffff'});
        this.title.anchor.setTo(0.5, 0.5);


        var btn_1Pgame_pos = {x: game.width/4, y: game.height-100};
        var btn_1Pgame = game.add.button(btn_1Pgame_pos.x, btn_1Pgame_pos.y, 'buttons', this.click_btn_1Pgame, this, 2, 0, 3);
        btn_1Pgame.anchor.setTo(0.5, 0.5);
        var btn_1Pgame_text = game.add.text(btn_1Pgame_pos.x, btn_1Pgame_pos.y, '1P Game', {font: '30px Arial', fill: '#ffffff'});
        btn_1Pgame_text.anchor.setTo(0.5, 0.5);

        var btn_2Pgame_pos = {x: game.width/4*3, y: game.height-100};
        var btn_2Pgame = game.add.button(btn_2Pgame_pos.x, btn_2Pgame_pos.y, 'buttons', this.click_btn_2Pgame, this, 2, 0, 3);
        btn_2Pgame.anchor.setTo(0.5, 0.5);
        var btn_2Pgame_text = game.add.text(btn_2Pgame_pos.x, btn_2Pgame_pos.y, '2P Game', {font: '30px Arial', fill: '#ffffff'});
        btn_2Pgame_text.anchor.setTo(0.5, 0.5);

        var btn_back_pos = {x: game.width/2, y: game.height-50};
        var btn_back = game.add.button(btn_back_pos.x, btn_back_pos.y, 'buttons', this.click_btn_back, this, 2, 0, 3);
        btn_back.anchor.setTo(0.5, 0.5);
        var btn_back_text = game.add.text(btn_back_pos.x, btn_back_pos.y, 'Back', {font: '30px Arial', fill: '#ffffff'});
        btn_back_text.anchor.setTo(0.5, 0.5);

    },

    update : function(){

    },




    click_btn_1Pgame : function(){


        game.state.start('level');
        game.global.prev_play_state = 'game_1p';
        // game.state.start('gamewin');
    },

    click_btn_2Pgame : function(){


        game.state.start('level');
        game.global.prev_play_state = 'game_2p';
        // game.state.start('gamewin');
    },

    click_btn_back : function(){


        game.state.start('menu');
    },
}