var bootState = {
    preload : function(){

        game.load.image('loading_bar', 'assets/loading_bar.png');
        
    },

    create : function(){
        game.stage.backgroundColor = "rgb(0, 0, 0)";
        game.physics.startSystem(Phaser.Physics.ARCADE);
        // game.physics.startSystem(Phaser.Physics.NINJA);
        // game.physics.ninja.gravity = 1;
        // game.renderer.renderSession.roundPixels = true;
        
        game.state.start('load');
    },
}