var loadState = {
    preload : function(){

        var loading_text = game.add.text(game.width/2, 150, 'loading...', {font: '30px Arial', fill: '#ffffff'});
        loading_text.anchor.setTo(0.5, 0.5);

        var loading_bar = game.add.sprite(game.width/2, 200, 'loading_bar');
        loading_bar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(loading_bar);




        // load assets
        /******************************************************/

        game.load.spritesheet('buttons', 'assets/buttons.png', 270, 38.2);
        game.load.atlas('player', 'assets/player.png', 'assets/player.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.atlas('bullet_red', 'assets/bullet_red.png', 'assets/bullet_red.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.atlas('part_red', 'assets/part_red.png', 'assets/part_red.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.image('enemy_1', 'assets/enemy_1.png');
        game.load.image('enemy_2', 'assets/enemy_2.png');
        game.load.image('shield', 'assets/shield.png');
        game.load.image('bullet_enemy', 'assets/bullet_green.png');
        game.load.spritesheet('explosion', 'assets/explosion.png', 89, 89);
        game.load.atlas('gameover_img', 'assets/gameover_img.png', 'assets/gameover_img.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.image('gameover_bg', 'assets/gameover_bg.jpg');
        game.load.image('game_bg1', 'assets/background1.png');
        game.load.image('game_bg2', 'assets/background2.png');
        game.load.image('game_bg3', 'assets/background3.png');
        game.load.image('hpbar_empty', 'assets/hpbar_empty.png');
        game.load.image('hpbar', 'assets/hpbar.png');
        game.load.atlas('boss1', 'assets/boss1.png', 'assets/boss1.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.image('win_bg', 'assets/win_bg.png');
        game.load.image('menu_bg', 'assets/menu_bg.png');
        // game.load.atlas('lightening', 'assets/lightening.png', 'assets/lightening.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.image('power', 'assets/power.png');
        game.load.image('pause_img', 'assets/pause_img.png');
        game.load.atlas('item_r', 'assets/item_r.png', 'assets/item_r.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.atlas('item_g', 'assets/item_g.png', 'assets/item_g.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.atlas('item_b', 'assets/item_b.png', 'assets/item_b.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
        game.load.image('speaker_down', 'assets/speaker_down.png');
        game.load.image('speaker_up', 'assets/speaker_up.png');
        game.load.image('window', 'assets/window.png');
        game.load.image('option_bg', 'assets/option_bg.png');
        game.load.image('score_bg', 'assets/score_bg.png');
        game.load.image('bullet_auto', 'assets/bullet_auto.png');




        game.load.audio('menu_bgm', 'assets/menu_bgm.ogg');
        game.load.audio('game_bgm', 'assets/game_bgm.ogg');
        game.load.audio('boss_bgm', 'assets/boss_bgm.ogg');

        game.load.audio('boss_show', 'assets/boss_show.ogg');
        game.load.audio('bullet_red_sound', 'assets/bullet_red_sound.ogg');
        game.load.audio('s_explode_sound', 'assets/s_explode_sound.ogg');
        game.load.audio('b_explode_sound', 'assets/b_explode_sound.ogg');
        game.load.audio('bullet_auto_sound', 'assets/bullet_auto_sound.ogg');
        
        
    },

    create : function(){
        game.global.menubgm = game.add.audio('menu_bgm');
        game.global.menubgm.volume = game.global.bgmvolume;

        game.global.gamebgm = game.add.audio('game_bgm');
        game.global.gamebgm.volume = game.global.bgmvolume;

        game.global.bossbgm = game.add.audio('boss_bgm');
        game.global.bossbgm.volume = game.global.bgmvolume;
        


        game.state.start('menu');
    },
}