var scoreState = {
    preload : function(){


        
    },

    create : function(){

        this.score_bg = game.add.sprite(game.width/2, game.height/2, 'score_bg');
        this.score_bg.anchor.setTo(0.5, 0.5);



        var test_text = game.add.text(game.width/2, 150, 'Leader Board', {font: '50px Arial', fill: '#ffffff'});
        test_text.anchor.setTo(0.5, 0.5);


        var btn_back_pos = {x: game.width/2, y: game.height-50};
        var btn_back = game.add.button(btn_back_pos.x, btn_back_pos.y, 'buttons', this.click_btn_back, this, 2, 0, 3);
        btn_back.anchor.setTo(0.5, 0.5);
        var btn_back_text = game.add.text(btn_back_pos.x, btn_back_pos.y, 'Back', {font: '30px Arial', fill: '#ffffff'});
        btn_back_text.anchor.setTo(0.5, 0.5);




        var postsRef = firebase.database().ref('leaderboard');
        var total_post = [];

        var text_1 = game.add.text(game.width/2, game.height/2-100, 'Loading', {font: '30px Arial', fill: '#ffffff'});
        text_1.anchor.setTo(0.5, 0.5);

        postsRef.once('value').then(function (snapshot){

            snapshot.forEach(function(child) {
                total_post.push(
                    {
                        name: child.val().name,
                        score: child.val().score,
                });
            });
            

            total_post.sort(function (a, b) {
                return a.score < b.score ? 1 : -1;
            });

            if(total_post.length==0){
                text_1.text = 'No data';
            }
            if(total_post.length>=1){
                text_1.text = '1st: ' + total_post[0].name + ' : ' + total_post[0].score;
            }
            if(total_post.length>=2){
                var text_2 = game.add.text(game.width/2, game.height/2-50, '2nd: ' + total_post[1].name + ' : ' + total_post[1].score, {font: '30px Arial', fill: '#ffffff'});
                text_2.anchor.setTo(0.5, 0.5);
            }
            if(total_post.length>=3){
                var text_3 = game.add.text(game.width/2, game.height/2, '3rd: ' + total_post[2].name + ' : ' + total_post[2].score, {font: '30px Arial', fill: '#ffffff'});
                text_3.anchor.setTo(0.5, 0.5);
            }
            if(total_post.length>=4){
                var text_4 = game.add.text(game.width/2, game.height/2+50, '4th: ' + total_post[3].name + ' : ' + total_post[3].score, {font: '30px Arial', fill: '#ffffff'});
                text_4.anchor.setTo(0.5, 0.5);
            }
            if(total_post.length>=5){
                var text_5 = game.add.text(game.width/2, game.height/2+100, '5th: ' + total_post[4].name + ' : ' + total_post[4].score, {font: '30px Arial', fill: '#ffffff'});
                text_5.anchor.setTo(0.5, 0.5);
            }
            
        });

    },

    update : function(){
        
    },




    click_btn_back : function(){


        game.state.start('menu');
    }
}